package com.mycorp;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;

import io.github.bonigarcia.wdm.BrowserManager;

@Deprecated
// Please use BrowserEnum with BrowserManagerFactory and WebDriverFactory
public enum BrowserManagerEnum {

    CHROME( "chrome" ),
    FIREFOX( "firefox" ),
    EDGE( "edge" ),
    IE( "ie" ),
    MARIONETTE( "marionette" ),
    OPERA( "opera" ),
    PHANTOMJS( "phantomjs" ),
    NONE( "test" );

    private final String browserName;

    private BrowserManagerFactory browserManagerFactory;
    private WebDriverFactory webDriverFactory;

    private BrowserManagerEnum( final String browserName ) {
        this.browserName = browserName;

        this.browserManagerFactory = new BrowserManagerFactory();
        this.webDriverFactory = new WebDriverFactory();
    }

    public static BrowserManagerEnum of( final String browserName ) {
        final String lBrowserName = StringUtils.lowerCase( browserName );
        for( final BrowserManagerEnum browser : BrowserManagerEnum.values() ) {
            if( browser.browserName.equals( lBrowserName ) ) {
                return browser;
            }
        }
        return NONE;
    }

    public BrowserManager getBrowserManager() {
        BrowserEnum be = BrowserEnum.of(this.browserName);
        return this.browserManagerFactory.getBrowserManager(be);
    }

    public BrowserManager getBrowserManager( final String version ) {
        return getBrowserManager().version( version );
    }

    public WebDriver getDriver() {
        BrowserEnum be = BrowserEnum.of(this.browserName);
        return this.webDriverFactory.getDriver(be);
    }
}
