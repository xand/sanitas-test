package com.mycorp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.github.bonigarcia.wdm.VoidDriverManager;


/**
 * Unit test for simple BrowserManagerEnumTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class BrowserManagerEnumTest {

    @Mock
    private WebDriverFactory webDriverFactory;

    @Mock
    private BrowserManagerFactory browserManagerFactory;

    @InjectMocks
    private BrowserManagerEnum ofChrome = BrowserManagerEnum.CHROME;

    @Before
    public void setup() {
        // se anulan todas las llamadas "de verdad"
        Mockito.when(this.webDriverFactory.getDriver(Mockito.any(BrowserEnum.class))).thenReturn(null);

        MockedBrowserManager mbm = new MockedBrowserManager();
        Mockito.when(this.browserManagerFactory.getBrowserManager(Mockito.any(BrowserEnum.class))).thenReturn(mbm);
    }

    @Test
    public void shouldCheckOf() throws Exception {
        BrowserManagerEnum of = BrowserManagerEnum.of( "test" );
        Assert.assertEquals( BrowserManagerEnum.NONE, of );
        of = BrowserManagerEnum.of( "perry" );
        Assert.assertEquals( BrowserManagerEnum.NONE, of );
        of = BrowserManagerEnum.of( null );
        Assert.assertEquals( BrowserManagerEnum.NONE, of );
        Assert.assertTrue( of.getBrowserManager() instanceof VoidDriverManager );
        Assert.assertTrue( of.getBrowserManager( "1" ) instanceof VoidDriverManager );
        Assert.assertTrue( of.getDriver() instanceof RemoteWebDriver );
    }

    @Test
    public void shouldCheckOfChrome() throws Exception {
        BrowserManagerEnum of = BrowserManagerEnum.of( "test" );
        Assert.assertEquals( BrowserManagerEnum.NONE, of );
        of = BrowserManagerEnum.of( "perry" );
        Assert.assertEquals( BrowserManagerEnum.NONE, of );

        // probamos Chrome

        Assert.assertEquals( BrowserManagerEnum.CHROME, ofChrome ); // esto no cambia

        // ahora bien, en vez de comprobar el tipo devuelto, vamos a comprobar que efectivamente se ha llamado
        // a BrowserManagerFactory.getBrowserManager() pasándole como argumento BrowserEnum.CHROME

        ofChrome.getBrowserManager();
        Mockito.verify(this.browserManagerFactory, Mockito.times(1)).getBrowserManager(BrowserEnum.CHROME);

        ofChrome.getBrowserManager("1");
        Mockito.verify(this.browserManagerFactory, Mockito.times(2)).getBrowserManager(BrowserEnum.CHROME);

        ofChrome.getDriver();
        Mockito.verify(this.webDriverFactory, Mockito.times(1)).getDriver(BrowserEnum.CHROME);
    }

}
