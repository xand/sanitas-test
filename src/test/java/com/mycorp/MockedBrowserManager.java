package com.mycorp;

import io.github.bonigarcia.wdm.BrowserManager;

import java.net.URL;
import java.util.List;

/**
 * Created by xand on 3/11/18.
 */
public class MockedBrowserManager extends BrowserManager {

    @Override
    protected List<URL> getDrivers() throws Exception {
        return null;
    }

    @Override
    protected String getExportParameter() {
        return null;
    }

    @Override
    protected String getDriverVersionKey() {
        return null;
    }

    @Override
    protected List<String> getDriverName() {
        return null;
    }

    @Override
    protected String getDriverUrlKey() {
        return null;
    }
}
