Fase 1:

A la hora de ejecutar los test tal cual la cobertura del código es de 79%.

Al desactivar la anotación @Ignore se produce un error y el informe no llega a ejecutarse. Este error es completamente lógico ya que se ejecuta en un entorno headless sin la posibilidad de ejecutar ningún navegador (por lo visto el desarrollador encargado del test desconoce el flag --headless ofrecido tanto por Chrome como por Firefox en las últimas versiones).

Por lo tanto, el primero objetivo consiste en poder ejecutar los tests y analizar la cobertura de código por los test.
Para ello se va a escoger un framework (Mockito) que nos permita mockear las llamadas a las clases/métodos no disponibles.

Por el otro lado el diseño de BrowserManagerEnum es totalmente incorrecto, ya que en un tipo enum (ni siquiera es una clase a pesar de que el enunciado del ejercicio lo llama así, para más info: https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html) se plantean dos factorías e implementación de una clase anónima que implementa la interfaz RemoteWebDriver.

Se propone el siguiente diseño que tiene en cuenta dichas carencias:
- Crear un tipo enumerado BrowserEnum que sólo contendrá los "navegadores" sin ninguna lógica de negocio.
- Crear la factoría BrowserManagerFactory.
- Crear la factoría WebDriverFactory.
- Crear la clase DefaultWebDriver extrayendo la lógica en el caso de un navegador de tipo "test".
- Por último se modifica el enumerado BrowserManagerEnum para ofrecer la retrocompatibilidad, pero teniendo en cuenta que esta clase se tiene que eliminar en las futuras versiones.
- Se marca BrowserManagerEnum como una clase deprecada.

Fase 2:

Con las modificaciones hechas en la Fase 1 podemos proceder a mockear las clases para realizar los test.
Para quitar todos los errores de golpe debido a que los objetos no pueden ser instanciados en un entorno determinado vamos a mockear todas las instanciaciones de dichos objetos con la clase MockedBrowserManager que tenga el método version(), con eso evitamos NullPointerException.

En vez de ello comprobaremos que efectivamente se realizan las llamadas a los métodos correspondientes.

Una vez terminada la fase 3 se ha conseguido subir el indicador de la cobertura a un 84%.

Se puede conseguir un indicador del 100% si se realizan los tests unitarios de todas las clases nuevas, pero queda fuera del ámbito de este ejercicio aparte de ser una tarea trivial con la refactorización realizada.

Fase 3:

Con la refactorización realizada aparece un nuevo problema: ahora todos los tests se ejecutan en todos los entornos con "navegadores" mockeados. Es decir, aunque se ejecute en un entorno gráfico, el resultado será siempre el mismo. Ver notas finales. Queda fuera del ámbito de este ejercicio proponer una solución.

Notas finales:

Dado el diseño de los tests queda evidente que no está nada clara la diferencia entre los tests funcionales (o de interfaz) y los unitarios. En este ejercicio en concreto se pretenden mezclar las dos cosas cuando hasta a nivel de maven existen dos tareas separadas para tal fin (test vs verify).

Básicamente, la razón principal es que no tiene ningún sentido (más allá del académico) el lanzar una serie de test en un entorno que de antemano se sabe que no va a ser capaz de ejecutarlos y al mismo tiempo preocuparse por el porcentaje de cobertura de dichos test demostrando, al mismo tiempo, falta de comprensión del significado de dicha métrica.





