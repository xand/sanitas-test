package com.mycorp;

import io.github.bonigarcia.wdm.*;

/**
 * Created by xand on 3/11/18.
 */
public class BrowserManagerFactory {

    public BrowserManager getBrowserManager(BrowserEnum browser) {
        switch(browser) {
            case CHROME:
                return ChromeDriverManager.getInstance().version( "2.24" );
            case FIREFOX:
                return FirefoxDriverManager.getInstance();
            case EDGE:
                return EdgeDriverManager.getInstance();
            case IE:
                return InternetExplorerDriverManager.getInstance();
            case MARIONETTE:
                return FirefoxDriverManager.getInstance();
            case OPERA:
                return OperaDriverManager.getInstance();
            case PHANTOMJS:
                return PhantomJsDriverManager.getInstance();
            case NONE: default:
                return VoidDriverManager.getInstance().version( "1" );
        }
    }

}
