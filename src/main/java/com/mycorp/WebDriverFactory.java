package com.mycorp;

import io.github.bonigarcia.wdm.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;

/**
 * Created by xand on 3/11/18.
 */
public class WebDriverFactory {

    public WebDriver getDriver(BrowserEnum browser) {
        switch(browser) {
            case CHROME:
                return new ChromeDriver();
            case FIREFOX:
                return new FirefoxDriver();
            case EDGE:
                return new EdgeDriver();
            case IE:
                return new InternetExplorerDriver();
            case MARIONETTE:
                return new FirefoxDriver();
            case OPERA:
                return new OperaDriver();
            case PHANTOMJS:
                return new PhantomJSDriver();
            case NONE: default:
                return new DefaultWebDriver();
        }
    }

}
