package com.mycorp;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Response;

import java.util.Map;

/**
 * Created by xand on 3/11/18.
 */
public class DefaultWebDriver extends RemoteWebDriver {

    private static final DesiredCapabilities DEFAULT_DESIRED_CAPABILITIES
            = new DesiredCapabilities( BrowserType.MOCK, "mock-version", Platform.ANY );

    public DefaultWebDriver() {
        super(DEFAULT_DESIRED_CAPABILITIES);
    }

    @Override
    protected Response execute( final String driverCommand, final Map< String, ? > parameters ) {
        return new Response();
    }

    @Override
    protected void startSession( final Capabilities desiredCapabilities, final Capabilities requiredCapabilities ) {
        setSessionId( "mock" );
    }

}
