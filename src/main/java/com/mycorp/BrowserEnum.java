package com.mycorp;

import io.github.bonigarcia.wdm.BrowserManager;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by xand on 3/11/18.
 */
public enum BrowserEnum {

    CHROME( "chrome" ),
    FIREFOX( "firefox" ),
    EDGE( "edge" ),
    IE( "ie" ),
    MARIONETTE( "marionette" ),
    OPERA( "opera" ),
    PHANTOMJS( "phantomjs" ),
    NONE( "test" );

    private final String browserName;

    private BrowserEnum(final String browserName) {
        this.browserName = browserName;
    }

    public String browserName() {
        return this.browserName;
    }

    public static BrowserEnum of( final String browserName ) {
        final String lBrowserName = StringUtils.lowerCase(browserName);
        for( final BrowserEnum browser : BrowserEnum.values() ) {
            if( browser.browserName.equals(lBrowserName) ) {
                return browser;
            }
        }
        return NONE;
    }

}
